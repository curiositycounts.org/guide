---
layout: default
title: Home
nav_order: 1
description: "Free learning for all."
permalink: /
---
# Welcome to CuriosityCounts.org
CuriosityCounts.org is a place to learn. For free! We believe in providing quality education to everyone, free of charge. CuriosityCounts.org is a non-profit organization, meaning our focus isn't earning money.
## Navigating CuriosityCounts.org
There are a few ways to navigate CuriosityCounts.org. Here they are:
### Use the search bar
Use the search bar to find pages quickly and easily. Just type in what you're looking for and results will pop up.
### Use your bookmarks (powered by CuriosityCounts.org Locker)
If you've bookmarked any pages before, you can access them in your CuriosityCounts.org Locker.
### Find what you're looking for in the menu
The menu shows several subjects. Click one and you'll see a list of topics. Click one of those, and you'll see a list of more specific topics. Then, you'll find even more specific topics with the actual pages inside. Some more complicated topics may have more topics inside them.
## Using CuriosityCounts.org Locker
Locker is your own personal spot on CuriosityCounts.org. You can access it with the 'Locker' button at the top menu. If you haven't already, you'll need to sign up to use Locker. Don't worry, it's free! It's not required so you can use CuriosityCounts.org, but it makes things a lot better. Here's what you'll get:
- **Progress tracking**: See how well you're learning and track your progress over time with unlimited progress tracking.
- **Bookmarks**: 'Bookmark' anything on CuriosityCounts.org and it's saved to your Locker.